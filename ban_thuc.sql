

CREATE DATABASE BanThuoc ON  PRIMARY 
	( NAME = 'BanThuoc', 
	FILENAME = 'C:\Users\khoin\Desktop\goPro\csdl\sql\BanThuoc.mdf' , 
	SIZE = 3072KB ,
	MAXSIZE = UNLIMITED, 
	FILEGROWTH = 1024KB )
 LOG ON 
	( NAME = 'BanThuoc_log', 
	FILENAME = 'C:\Users\khoin\Desktop\goPro\csdl\sql\BanThuoc.ldf' , 
	SIZE = 1024KB , 
	MAXSIZE = 2048KB , 
	FILEGROWTH = 10%);
Go

--2.	Create tables
Create table NhanVien(
	MaNV char(10) NOT NULL,
	ViTri varchar(15) NOT NULL,
	HoTen varchar(30) NOT NULL,
	DiaChi varchar(30) NULL,
	NoiSinh varchar(30) NULL,
	NgaySinh datetime NULL);
go

Create Table KhachHang(
	MaKH char(10) NOT NULL,
	TenKH varchar(30) NOT NULL,
	NguoiDaiDien varchar(30) NOT NULL,
	DiaChi varchar(30) NULL,
	Loai varchar(15) NOT NULL,
	Sdt varchar(10) NULL,
	Fax varchar(10) NULL,
	Email varchar(30) NULL);
go

Create table HopDong(
	ID char(10) NOT NULL,
	NgayLap datetime NOT NULL,
	HieuLuc datetime NOT NULL,
	MaNV char(10) NOT NULL,
	MaKH char(10) NOT NULL);
go

Create table PhieuGiao(
	ID char(10) NOT NULL,
	HD_ID char(10) NOT NULL,
	MaNV char(10) NOT NULL,	
	NgayGiao datetime NOT NULL);
go

Create table PhieuThu(
	ID char(10) NOT NULL,
	PG_ID char(10) NOT NULL,
	MaNV char(10) NOT NULL,	
	NgayLap datetime NOT NULL,
	SoTien DECIMAL(10, 2) );
go

Create table DuocPham(
	ID char(10) NOT NULL,
	Ten varchar(30) NOT NULL,
	DonVi varchar(10) NOT NULL,
	Gia  DECIMAL(10, 2),
	Loai char(10) NOT NULL);
go

Create table HD_ChiTiet(
	Dp_ID char(10) NOT NULL,
	HD_ID char(10) NOT NULL,
	DonGia varchar(10) NOT NULL,
	SoLuong int NUll );
go

Create table PG_ChiTiet(
	Dp_ID char(10) NOT NULL,
	PG_ID char(10) NOT NULL,
	SoLuong int NUll,
	HanSD datetime NOT NULL);
go




--3.	Add Primary key

ALTER TABLE NhanVien ADD Constraint pk_MaNV PRIMARY KEY (MaNV);
GO
	ALTER TABLE KhachHang ADD Constraint pk_MaKH PRIMARY KEY (MaKH);
GO
	ALTER TABLE HopDong ADD Constraint pk_HD PRIMARY KEY (ID);
GO
	ALTER TABLE PhieuGiao ADD Constraint pk_PG PRIMARY KEY (ID);
GO
	ALTER TABLE PhieuThu ADD Constraint pk_PT PRIMARY KEY (ID);
GO
	ALTER TABLE DuocPham ADD Constraint pk_DP PRIMARY KEY (ID);
GO
	ALTER TABLE HD_ChiTiet ADD Constraint pk_HD_CT PRIMARY KEY (DP_ID,HD_ID);
GO
	ALTER TABLE PG_ChiTiet ADD Constraint pk_HD_CT PRIMARY KEY (DP_ID,PG_ID);
GO




--4.	Add Foreign key
-----Foreign key
------------
ALTER TABLE HopDong
	ADD constraint fk_HD_Nv FOREIGN KEY(MaNv)
	REFERENCES NhanVien(MaNV);

ALTER TABLE HopDong
	ADD constraint fk_HD_KH FOREIGN KEY(MaKH)
	REFERENCES KhachHang(MaKH);
-----------------

ALTER TABLE PhieuGiao
	ADD constraint fk_PG_Nv FOREIGN KEY(MaNv)
	REFERENCES NhanVien(MaNV);

ALTER TABLE PhieuGiao
	ADD constraint fk_PG_HD FOREIGN KEY(HD_ID)
	REFERENCES HopDong(ID);
--------------
ALTER TABLE PhieuThu
	ADD constraint fk_PT_Nv FOREIGN KEY(MaNv)
	REFERENCES NhanVien(MaNV);

ALTER TABLE PhieuThu
	ADD constraint fk_PT_PG FOREIGN KEY(PG_ID)
	REFERENCES PhieuGiao(ID);
------------------
ALTER TABLE HD_ChiTiet
	ADD constraint fk_HDCT_DP FOREIGN KEY(DP_ID)
	REFERENCES DuocPham(ID);

ALTER TABLE HD_ChiTiet
	ADD constraint fk_HDCT_HD FOREIGN KEY(HD_ID)
	REFERENCES HopDong(ID);
-------------------------
ALTER TABLE PG_ChiTiet
	ADD constraint fk_PGCT_DP FOREIGN KEY(DP_ID)
	REFERENCES DuocPham(ID);

ALTER TABLE PG_ChiTiet
	ADD constraint fk_PGCT_PG FOREIGN KEY(PG_ID)
	REFERENCES PhieuGiao(ID);
--  Data Demo for NhanVien table
INSERT INTO NhanVien (MaNV, ViTri, HoTen, DiaChi, NoiSinh, NgaySinh)
VALUES 
  ('NV001', 'Quan ly', 'Nguyen Van A', '123 Street, City', 'Hanoi', '1990-01-01'),
  ('NV002', 'Nhan vien', 'Tran Thi B', '456 Street, City', 'Hue', '1995-05-15'),
  ('NV003', 'Nhan vien', 'Le Van C', '789 Road, City', 'Da Nang', '1992-03-05'),
  ('NV004', 'Quan ly', 'Pham Thi D', '234 Lane, City', 'Ho Chi Minh', '1988-09-12'),
  ('NV005', 'Nhan vien', 'Tran Van E', '1234 Lane, City', 'Can Tho', '1993-06-20');

--  Data Demo for KhachHang table
INSERT INTO KhachHang (MaKH, TenKH, NguoiDaiDien, DiaChi, Loai, Sdt, Fax, Email)
VALUES 
  ('KH001', 'ABC Company', 'Mr. X', '789 Avenue, City', 'Doanh nghiep', '1234567890', '9876543210', 'abc@example.com'),
  ('KH002', 'XYZ Corporation', 'Ms. Y', '987 Boulevard, City', 'Cong ty TNHH', '0987654321', '1234567890', 'xyz@example.com'),
  ('KH003', 'EFG Corporation', 'Mr. Z', '345 Street, City', 'Cong ty co phan', '0987654321', '1234567890', 'efg@example.com'),
  ('KH004', 'HIJ Ltd.', 'Ms. W', '567 Avenue, City', 'Doanh nghiep', '1234567890', '9876543210', 'hij@example.com'),
  ('KH005', 'KLM Company', 'Mr. L', '910 Road, City', 'Cong ty TNHH', '0987654321', '1234567890', 'klm@example.com');

-- Data Demo for HopDong table
INSERT INTO HopDong (ID, NgayLap, HieuLuc, MaNV, MaKH)
VALUES 
  ('HD001', '2023-01-01', '2023-12-31', 'NV001', 'KH001'),
  ('HD002', '2023-02-15', '2023-11-30', 'NV002', 'KH002'),
  ('HD003', '2023-03-01', '2023-12-31', 'NV003', 'KH003'),
  ('HD004', '2023-04-10', '2023-11-30', 'NV004', 'KH004'),
  ('HD005', '2023-06-01', '2023-12-31', 'NV005', 'KH005');

-- Data Demo for PhieuGiao table
INSERT INTO PhieuGiao (ID, HD_ID, MaNV, NgayGiao)
VALUES 
  ('PG001', 'HD001', 'NV001', '2023-02-01'),
  ('PG002', 'HD002', 'NV002', '2023-03-15'),
  ('PG003', 'HD003', 'NV003', '2023-04-01'),
  ('PG004', 'HD004', 'NV004', '2023-05-15'),
  ('PG005', 'HD005', 'NV005', '2023-07-01');

-- Data Demo for PhieuThu table
INSERT INTO PhieuThu (ID, PG_ID, MaNV, NgayLap, SoTien)
VALUES 
  ('PT001', 'PG001', 'NV001', '2023-02-10', 1500000),
  ('PT002', 'PG002', 'NV002', '2023-03-20', 2000000),
  ('PT003', 'PG003', 'NV003', '2023-04-10', 1800000),
  ('PT004', 'PG004', 'NV004', '2023-05-20', 2200000),
  ('PT005', 'PG005', 'NV005', '2023-07-10', 2000000);

-- Data Demo for DuocPham table
INSERT INTO DuocPham (ID, Ten, DonVi, Gia, Loai)
VALUES 
  ('DP001', 'Paracetamol', 'Vi', 5.50, 'Thuoc ha sot'),
  ('DP002', 'Aspirin', 'Vi', 3.75, 'Thuoc giam dau'),
  ('DP003', 'Ibuprofen', 'Vi', 8.25, 'Thuoc chong viem'),
  ('DP004', 'Cefalexin', 'Vi', 6.50, 'Khang sinh'),
  ('DP005', 'Amoxicillin', 'Vi', 7.00, 'Khang sinh');

-- Data Demo for HD_ChiTiet table
INSERT INTO HD_ChiTiet (DP_ID, HD_ID, DonGia, SoLuong)
VALUES 
  ('DP001', 'HD001', '5.50', 100),
  ('DP002', 'HD002', '3.75', 50),
  ('DP003', 'HD003', '8.25', 80),
  ('DP004', 'HD004', '6.50', 60),
  ('DP005', 'HD005', '7.00', 90);

-- Data Demo for PG_ChiTiet table
INSERT INTO PG_ChiTiet (DP_ID, PG_ID, SoLuong, HanSD)
VALUES 
  ('DP001', 'PG001', 50, '2023-03-01'),
  ('DP002', 'PG002', 30, '2023-04-01'),
  ('DP003', 'PG003', 40, '2023-06-01'),
  ('DP004', 'PG004', 25, '2023-07-01'),
  ('DP005', 'PG005', 60, '2023-09-01');

 
 
-----------------------sql--------------------------
select * from dbo.DuocPham
select * from dbo.HD_ChiTiet
select * from dbo.HopDong
select * from dbo.KhachHang
select * from dbo.NhanVien
select * from dbo.PG_ChiTiet
select * from dbo.PhieuGiao
select * from dbo.PhieuThu
----------------------------------------------------
--Cho biet ID hop dong , ma khach hang , ngay lap cua hop dong duoc lap boi nvA
SELECT HD.HD_ID,HD.MaKH,HD.NgayLap, NV.HoTen
FROM HopDong HD
JOIN NhanVien NV ON HD.MaNV= NV.MaNV
where NV.HoTen = 'Nguyen Van A'

-- chi tiet hop dong thuoc ve khach hang co ma so 'KH001' va co duoc pham mang ma so 'DP001'
select *
from HopDong HD
join HD_ChiTiet HDCT on HDCT.HD_ID = HD.HD_ID
join KhachHang KH on HD.MaKH = KH.MaKH
where KH.MaKH = 'KH001'
and Dp_ID = 'DP001'

-- Thong tin phieu giao, phieu thu co duoc pham co ma so 'DP001' va so luong <50
select PG.PG_ID,PG.HD_ID,PG.MaNV,Dp_ID,SoLuong,NgayGiao,SoTien,PT.PG_ID,PT.MaNV,PT.NgayLap,PT.SoTien
from PhieuGiao PG
join PG_ChiTiet PGCT on PG.PG_ID = PGCT.PG_ID
join PhieuThu PT on PG.PG_ID = PT.PG_ID
where Dp_ID ='DP001'
AND SoLuong <=50
--
-- USE EXEC to rename column 
EXEC sp_rename 'HopDong.ID', 'HD_ID';
EXEC sp_rename 'PhieuGiao.ID', 'PG_ID';
EXEC sp_rename 'PhieuThu.ID', 'PT_ID';
EXEC sp_rename 'DuocPham.ID', 'DP_ID';
-- Tong thu 
SELECT PT.MaNV,NV.HoTen, SUM(PT.SoTien) AS TongSoTienDaNhanThanhToan
FROM PhieuThu PT
join NhanVien NV on PT.MaNV = NV.MaNV
group by PT.MaNV,NV.HoTen
-- tong so thuoc da ban
SELECT DP.Loai, SUM(HDCT.SoLuong) AS TongSoLuong
FROM DuocPham DP
JOIN HD_ChiTiet HDCT ON DP.DP_ID = HDCT.DP_ID
GROUP BY DP.Loai;








